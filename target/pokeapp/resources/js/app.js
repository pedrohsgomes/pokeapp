angular
    .module('pokeApp', ['ngResource'])
    .service('TesteService', function ($log, $resource) {
        return {
            getAll: function () {
                var testeResource = $resource('teste', {}, {
                    query: {method: 'GET', params: {}, isArray: true}
                });
                return testeResource.query();
            }
        }
    })
    .controller('TesteController', function ($scope, $log, TesteService) {
        $scope.teste = TesteService.getAll();
    });
